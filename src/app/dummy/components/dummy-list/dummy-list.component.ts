import { Component, OnInit } from '@angular/core';
import { DummyService } from "../../services/dummy.service";
import { MatTableDataSource } from "@angular/material/table";
import { FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-dummy-list',
  templateUrl: './dummy-list.component.html',
  styleUrls: ['./dummy-list.component.scss']
})
export class DummyListComponent implements OnInit {
  filterForm: FormGroup = new FormGroup({
    filterElement: new FormControl(null)
  });
  displayedColumns = ['item', 'length'];
  datasource: MatTableDataSource<{ item: string, length: number }> = new MatTableDataSource<{ item: string; length: number }>(
    [
      { item: 'fuzz', length: 4 },
      { item: 'bizz', length: 4 }
    ]
  );

  constructor(
    private dummyService: DummyService,
  ) {
    this.datasource.filterPredicate = (value, filterString) => {
      return value.item.includes(filterString);
    };
  }

  ngOnInit(): void {
    this.dummyService.getTransformedDummyList().subscribe(data => {
      this.datasource.data = data.items;
    });
  }

  public filterData() {
    this.datasource.filter = this.filterForm.controls['filterElement'].value;
  }
}
