import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { DummyListComponent } from './dummy-list.component';
import { DummyService } from "../../services/dummy.service";
import { Observable, of } from "rxjs";
import { TransformedDummyList } from "../../services/models/DummyList";
import { TranslateTestingModule } from "ngx-translate-testing";
import { MatTableModule } from "@angular/material/table";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../../shared/shared.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";
import { TestbedHarnessEnvironment } from "@angular/cdk/testing/testbed";
import { HarnessLoader } from "@angular/cdk/testing";
import { MatButtonHarness } from "@angular/material/button/testing";
import { MatInputHarness } from "@angular/material/input/testing";

const mockItems: {item: string; length: number;}[] = [
  { item: 'fizz', length: 4 },
  { item: 'buzz', length: 4 }
];

class MockDummyService {
  getTransformedDummyList(): Observable<TransformedDummyList> {
    return of({
      items: mockItems
    } as TransformedDummyList);
  }
}

describe('DummyListComponent', () => {
  let component: DummyListComponent;
  let fixture: ComponentFixture<DummyListComponent>;
  let dummyService: DummyService;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DummyListComponent
      ],
      providers: [
        { provide: DummyService, useClass: MockDummyService },
      ],
      imports: [
        TranslateTestingModule.withTranslations({ en: require('@assets/i18n/en.json') }),
        MatTableModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        ReactiveFormsModule,
        SharedModule,
        BrowserAnimationsModule,
      ]
    })
      .compileComponents();
    dummyService = TestBed.inject(DummyService);
    // TestBed.inject(HttpClient)
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have table datasource holding proper data', fakeAsync(() => {
    spyOn(dummyService, 'getTransformedDummyList').and.returnValue(of({ items: mockItems }));

    expect(component.datasource.data).toEqual(mockItems);
  }));

  it('should have a table datasource filtering the data based on partial include', fakeAsync(() => {
    spyOn(dummyService, 'getTransformedDummyList').and.returnValue(of({ items: mockItems }));
    component.filterForm.controls['filterElement'].setValue('fizz');
    component.filterData();
    expect(component.datasource.filteredData).toEqual([{ item: 'fizz', length: 4 }]);
  }));

  it(' should have a filter with a corresponding input, button and a table', () => {
    const rootElement: DebugElement = fixture.debugElement;
    expect(rootElement.query(By.css('.dummy-table-filter-container input[ng-reflect-name=filterElement]'))).toBeTruthy();
    expect(rootElement.query(By.css('.dummy-table-filter-container button[type=submit]'))).toBeTruthy();
    expect(rootElement.query(By.css('table.dummy-table-container'))).toBeTruthy();
  });

  it('should have a filtered tablewhen the filterElement field is set and filter button is pressed', async () => {
    spyOn(dummyService, 'getTransformedDummyList').and.returnValue(of({ items: mockItems }));
    expect(component.datasource.data).toEqual(mockItems);

    const filterInput =await loader.getHarness(MatInputHarness.with({ selector: '.dummy-table-filter-container input[ng-reflect-name=filterElement]' }));
    const submitButtonLoader = await loader.getChildLoader('.dummy-table-filter-container');
    const submitButton = await submitButtonLoader.getHarness(MatButtonHarness.with({ selector: 'button[type=submit]' }));

    const tableRows = fixture.debugElement.queryAll(By.css('table.dummy-table-container tbody tr'));
    expect(tableRows.length).toEqual(2);

    await filterInput.setValue('fizz');

    await submitButton.click();
    fixture.detectChanges();
    const newTableRows = fixture.debugElement.queryAll(By.css('table.dummy-table-container tbody tr'));
    expect(newTableRows.length).toEqual(1);

  });
});
