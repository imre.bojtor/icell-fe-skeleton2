import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { DummyListComponent } from './components/dummy-list/dummy-list.component';
import { DummyRoutingModule } from "./dummy-routing.module";
import { MatTableModule } from "@angular/material/table";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";



@NgModule({
  declarations: [
    DummyListComponent,
  ],
  imports: [
    SharedModule,
    DummyRoutingModule,
    MatTableModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class DummyModule { }
