import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { DummyListComponent } from "./components/dummy-list/dummy-list.component";

const routes: Routes = [
  {
    path: '',
    component: DummyListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DummyRoutingModule { }
