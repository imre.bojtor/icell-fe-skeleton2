import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

import { DummyService } from './dummy.service';

describe('DummyService', () => {
  let service: DummyService;
  let httpMock: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(DummyService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have a getDummyList method  calling GET endpoint and returning data unmodified ', (done) => {
    const dummyList = { items: ['fizz', 'buzz'] };
    service.getDummyList().subscribe((list)=>{
      expect(list).toEqual(dummyList);
      done();
    });
    httpMock.expectOne('/dummies').flush(dummyList);
    httpMock.verify();
  });

  it('should have a getTransformedDummyList method calling GET endpoint and transforming data', () => {
    const dummyList = { items: ['fizz', 'buzz'] };
    service.getTransformedDummyList().subscribe((list)=>{
      expect(list).toEqual({
        items: [
          { item: 'fizz', length: 4 },
          { item: 'buzz', length: 4 }
        ]
      });
    });

    httpMock.expectOne('/dummies').flush(dummyList);
    httpMock.verify();
  });

});
