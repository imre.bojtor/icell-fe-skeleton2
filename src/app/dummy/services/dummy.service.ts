import { Injectable } from '@angular/core';
import { map, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { DummyList, TransformedDummyList } from "./models/DummyList";

@Injectable({
  providedIn: 'root'
})
export class DummyService {

  constructor(private httpClient: HttpClient) { }

  public getDummyList(): Observable<DummyList> {
    return this.httpClient.get<DummyList>('/dummies');
  }

  public getTransformedDummyList(): Observable<TransformedDummyList> {
    return this.httpClient.get<DummyList>('/dummies').pipe(map(dl => {
      return {
        items: dl.items.map(item => {
          return {
            item,
            length: item.length
          };
        })
      } as TransformedDummyList;
    }));
  }



}
