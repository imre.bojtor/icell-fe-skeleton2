
export interface DummyList {
  items: string[]
}

export interface TransformedDummyList {
  items: {item: string, length: number}[]
}
