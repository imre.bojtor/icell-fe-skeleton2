import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { TranslateModule } from "@ngx-translate/core";
import { ReactiveFormsModule } from "@angular/forms";

//

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TranslateModule.forChild({
      defaultLanguage: 'en',
      isolate: false,
      extend: true,
    }
    )
  ],
  exports: [
    HttpClientModule,
    TranslateModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
