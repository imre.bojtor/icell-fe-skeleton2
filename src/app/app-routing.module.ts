import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'dummies',
    loadChildren: () => import('./dummy/dummy.module').then(m => m.DummyModule)
  },
  {
    path: '',
    redirectTo: 'dummies',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
